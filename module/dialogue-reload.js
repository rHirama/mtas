import { createShortActionMessage } from "./chat.js";

export class ReloadDialogue extends Application {
  constructor(item, button, container, ...args) {
    super(...args);
    this._item = item;
    this._button = button;
    this.container = container;
    console.trace(this._item);
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue"],
  	  template: "systems/mtas/templates/dialogues/dialogue-reload.html",
      width: 200,
      height: 400
    });
  }
  
  getData() {
    const data = super.getData();
    
    let owner = this._item.actor;
    if(owner == null){
      console.log("Error: no actor found.");
      return data;
    }
    
    const ammo_type = this._item.data.data.cartridge;
    data.ammoList = owner.data.items.filter(element => (element.type === "ammo") && (element.data.cartridge === ammo_type) && (element.data.quantity > 0));
    
    return data;
  }
  
  activateListeners(html) {
    super.activateListeners(html);

    // Select Ammo
    html.find('.selectAmmo').click(ev => this._onSelectAmmo(event));

  }
  
  
  async _onSelectAmmo(){  
    if(!this._button.classList.contains("reloaded")) this._button.classList.add("reloaded");

    const itemId = event.currentTarget.closest(".selectAmmo").dataset.itemId;
    const ammo = this._item.actor.getOwnedItem(itemId);
    const owner = this._item.actor;
    
    //createShortActionMessage("Reloads", owner);
   

    //Calculate the ammo amount that is reloaded and substract it from the inventory item 
    const newAmmo = new Item(ammo.data, {temporary: true});
    const magazineSize = this._item.data.data.capacity;
    const ammoAmount = ammo.data.data.quantity;
    let transferAmount = ammoAmount - magazineSize;
    let updateData = [];
    if(transferAmount <= 0){
      //Deleting old ammo
      transferAmount = ammoAmount;
      await owner.deleteOwnedItem(ammo.data._id);
    } 
    else{
      transferAmount = magazineSize;
      const newAmount = ammoAmount - transferAmount;
      updateData.push({_id: ammo.data._id,"data.quantity": newAmount});
    } 

    newAmmo.data.data.quantity = transferAmount;
    updateData.push({_id: this._item.data._id, "data.magazine": newAmmo});
    await owner.updateOwnedItem(updateData);

    if(this.container) this.container.render();
    this.close();
  }
}