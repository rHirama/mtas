/**
 * Define a set of template paths to pre-load
 * Pre-loaded templates are compiled and cached for fast access when rendering
 * @return {Promise}
 */
export const preloadHandlebarsTemplates = async function() {

  // Define template paths to load
  const templatePaths = [
    // Actor Sheet Partials
    "systems/mtas/templates/actors/parts/vamp-disciplines.html",
    "systems/mtas/templates/actors/parts/mage-magic.html",
    "systems/mtas/templates/actors/parts/base-inventory.html",
    "systems/mtas/templates/actors/parts/changeling-powers.html"
  ];

  // Load the template parts
  return loadTemplates(templatePaths);
};
