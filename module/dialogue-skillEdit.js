export class SkillEditDialogue extends Application {
  constructor(actor, skill_key, skill_type,...args) {
    super(...args);
    this.actor = actor;
    this.skill_key = skill_key;
    this.skill_type = skill_type;
    this.stats = actor.data.data;
    this.dataString;
    
    if (this.stats.abilities[this.skill_type][this.skill_key] === undefined){
      this.stats = duplicate(this.stats.attributes);
      this.dataString = 'data.attributes';
    } else {
      this.stats = duplicate(this.stats.abilities);
      this.dataString = 'data.abilities';
    }
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue", "mtas-sheet"],
  	  template: "systems/mtas/templates/dialogues/dialogue-skillEdit.html",
      resizable: true,
      width: 240,
      height: 160,
      title: "Specialties"
    });
  }
  
  getData() {
    const data = super.getData();
    data.specialties = this.stats[this.skill_type][this.skill_key].specialties;
    
    return data;
  }
  

  activateListeners(html) {
    super.activateListeners(html);
    
    html.find('.add-specialty').click(ev => this._onAddSpecialty(ev, this.stats, this.dataString));
    html.find('.remove-specialty').click(ev => this._onRemoveSpecialty(ev, this.stats, this.dataString));
    html.find('input').change(ev => this._onChangeSpecialty(ev, this.stats, this.dataString));
  }

  async _onAddSpecialty(event, stats, dataString) {
    if(!stats[this.skill_type][this.skill_key].specialties) stats[this.skill_type][this.skill_key].specialties = [];
    stats[this.skill_type][this.skill_key].specialties.push("New Specialty");

    let obj = {}
    obj[dataString] = stats;
    await this.actor.update(obj)
    this.render();
  }
  
  async _onRemoveSpecialty(event, stats, dataString) {
    const specialty_index = event.currentTarget.dataset.index;
    if(!stats[this.skill_type][this.skill_key].specialties) stats[this.skill_type][this.skill_key].specialties = [];
    stats[this.skill_type][this.skill_key].specialties.splice(specialty_index, 1);

    let obj = {}
    obj[dataString] = stats;
    await this.actor.update(obj)
    this.render();
  }
  
  async _onChangeSpecialty(event, stats, dataString) {
    const specialty_index = event.currentTarget.dataset.index;
    const specialty_value = event.currentTarget.value;
    stats[this.skill_type][this.skill_key].specialties[specialty_index] = specialty_value;

    let obj = {}
    obj[dataString] = stats;
    await this.actor.update(obj)
    this.render();
  }
  
  async _onToggleAsset(event, stats, dataString) {
    stats[this.skill_type][this.skill_key].isAssetSkill = !stats[this.skill_type][this.skill_key].isAssetSkill;

    let obj = {}
    obj[dataString] = stats;
    await this.actor.update(obj)
    this.render();
  }

}