export class DiceRollerDialogue extends Application {
  constructor({dicePool=0, targetNumber=8, flavor="Ability Check", addBonusFlavor=false, title="Ability Check", blindGMRoll=false}, ...args){
    super(...args);
    this.targetNumber = +targetNumber;
    this.dicePool = +dicePool;
    this.flavor = flavor;
    this.addBonusFlavor = addBonusFlavor;
    this.blindGMRoll = blindGMRoll;
    this.options.title = title;
  }

  /* -------------------------------------------- */

  /**
   * Extend and override the default options used by the 5e Actor Sheet
   * @returns {Object}
   */
	static get defaultOptions() {
	  return mergeObject(super.defaultOptions, {
  	  classes: ["worldbuilding", "dialogue", "mtas-sheet"],
  	  template: "systems/mtas/templates/dialogues/dialogue-diceRoller.html",
      resizable: true
    });
  }
  
  getData() {
    const data = super.getData();
    data.targetNumber = this.targetNumber;
    data.dicePool = this.dicePool;
    data.bonusDice = 0;
    
    return data;
  }
  
  _fetchInputs(html){
    const dicePool_userMod_input = html.find('[name="dicePoolBonus"]');
    
    let dicePool_userMod = dicePool_userMod_input.length ? +dicePool_userMod_input[0].value : 0;
    let explode_threshold = Math.max(0,+($('input[name=explodeThreshold]:checked').val()));
    let rote_action = $('input[name=rote_action]').prop("checked");
    
    return {dicePool_userMod: dicePool_userMod, explode_threshold: explode_threshold, rote_action: rote_action}
  }
  
  activateListeners(html) {
    super.activateListeners(html);
    
    html.find('.roll-execute').click(ev => {  
      const modifiers = this._fetchInputs(html);
      let dicePool = this.dicePool + modifiers.dicePool_userMod;
      let roteAction = modifiers.rote_action;
      let flavor = this.flavor ? this.flavor : "Ability Check";
      flavor += modifiers.dicePool_userMod>0 ? " + " + modifiers.dicePool_userMod : modifiers.dicePool_userMod<0 ? " - " + -modifiers.dicePool_userMod : "";
      let explodeThreshold = modifiers.explode_threshold;
      
      DiceRollerDialogue.rollToChat({dicePool: dicePool, tenAgain: explodeThreshold===10, nineAgain: explodeThreshold===9, eightAgain: explodeThreshold===8, roteAction: roteAction, flavor: flavor, blindGMRoll: this.blindGMRoll});
    });
  }
  
  
  static _roll({dicePool=1, tenAgain=true, nineAgain=false, eightAgain=false, roteAction=false, chanceDie=false}){
    //Create dice pool qualities
    const roteActionString = roteAction ? "r<8" : "";
    const explodeString = eightAgain ? "x>=8" : nineAgain ? "x>=9" : tenAgain ? "x>=10" : "" ;
    const targetNumString = chanceDie ? "cs>=10" : "cs>=8";
    
    let roll = new Roll("(@dice)d10" + roteActionString + explodeString + targetNumString, {
      dice: dicePool
    }).roll();
    
    console.trace(roll);
    if(chanceDie && roteAction && roll.terms[0].results[0].result === 1){
      //Chance dice don't reroll 1s with Rote quality
      roll.terms[0].results.splice(1);
    }
    return roll;
  }
  
  
  static rollToHtml({dicePool=1, tenAgain=true, nineAgain=false, eightAgain=false, roteAction=false, flavor="Ability Check"}){   
    //Is the roll a chance die?
    let chanceDie = false;
    if(dicePool < 1) {
      tenAgain = false;
      chanceDie = true;
      dicePool = 1;
    }
    
    let roll = DiceRollerDialogue._roll({dicePool: dicePool, tenAgain: tenAgain, nineAgain: nineAgain, eightAgain: eightAgain, roteAction: roteAction, chanceDie: chanceDie});
    
    //Create Roll Message
    let speaker = ChatMessage.getSpeaker();
    
    if(chanceDie) flavor += " [Chance die]";
    if(roteAction) flavor += " [Rote quality]";
    if(eightAgain) flavor += " [8-again]";
    else if(nineAgain) flavor += " [9-again]";
    else if(tenAgain) flavor += " [10-again]";
    
    
    return roll.render({speaker: speaker, flavor: flavor })
  }

  
  static rollToChat({dicePool=1, tenAgain=true, nineAgain=false, eightAgain=false, roteAction=false, flavor="Ability Check", blindGMRoll=false}){
    //Is the roll a chance die?
    let chanceDie = false;
    if(dicePool < 1) {
      tenAgain = false;
      chanceDie = true;
      dicePool = 1;
    }
    
    let roll = DiceRollerDialogue._roll({dicePool: dicePool, tenAgain: tenAgain, nineAgain: nineAgain, eightAgain: eightAgain, roteAction: roteAction, chanceDie: chanceDie});
    
    //Create Roll Message
    let rollMode = blindGMRoll ? "blindroll" : game.settings.get("core", "rollMode");
    let speaker = ChatMessage.getSpeaker();
    
    if(chanceDie) flavor += " [Chance die]";
    if(roteAction) flavor += " [Rote quality]";
    if(eightAgain) flavor += " [8-again]";
    else if(nineAgain) flavor += " [9-again]";
    else if(tenAgain) flavor += " [10-again]";
    
    
    // Convert the roll to a chat message and return the roll
    return roll.toMessage({
      speaker: speaker,
      flavor: flavor
    }, { rollMode });
  }
  
  //Deprecated
  static createDiceRollMessage({dicePool=1, targetNumber=8, explodeThreshold=10, roteAction=false, blindGMRoll=false, chanceDie=false}={}, flavor="Ability Check", addBonusFlavor, bonus){
      if(+targetNumber > 0){
        //Roll
        if(+dicePool < 1){
          dicePool = 1;
          chanceDie = true;
        }
        if(chanceDie) targetNumber = 10;
        
        let roteActionString = roteAction ? "r<8" : "";
        if(chanceDie && explodeThreshold===10) explodeThreshold = 11;
        let explodeString = explodeThreshold<=10 ? "x>=" + explodeThreshold : "";
        
        let roll = new Roll("(@dice)d10" + roteActionString + explodeString + "cs>=(@diff)", {
          dice: dicePool,
          diff: targetNumber
        }).roll();
        
        console.trace(roll);
        if(chanceDie && roteAction && roll._dice[0].rolls[0].roll === 1){
          roll._dice[0].rolls.splice(1);
        }
        
        //Create Roll Message
        let rollMode = blindGMRoll ? "blindroll" : game.settings.get("core", "rollMode");
        let speaker = ChatMessage.getSpeaker();
        
        if(addBonusFlavor) flavor += " + " + bonus;
        if(chanceDie) flavor += " [Chance die]";
        if(roteAction) flavor += " [Rote quality]";
        if(explodeThreshold === 8) flavor += " [8-again]";
        else if(explodeThreshold === 9) flavor += " [9-again]";
        else if(explodeThreshold === 10) flavor += " [10-again]";
        
        // Convert the roll to a chat message and return the roll
        return roll.toMessage({
          speaker: speaker,
          flavor: flavor
        }, { rollMode });
      }
    else console.log("ERROR: target number is 0 or lower!");
  }

}