# Mage the Ascension for FoundryVTT

**ATTENTION! Please make sure you update your system. It should show 0.5.0 (or higher) as the version number!**

This is an adaptation of the Mage the Awakening system for FoundryVTT (available at: https://gitlab.com/MarlQ/mta) to Mage the Ascension.

Manifest: https://gitlab.com/rHirama/mtas/-/raw/master/system.json

If someone wants to help with development, you're always welcome.
I'll continue working on this until I'm done with my current campaign,
though if people are interesting I might continue afterwards.

Dev branch will possibly be unstable, homebrew branch is for my personal homebrew and the place where most features originate.


### Images (click for full-size)

[<img src="/screenshots/characterSheet.png" width="128" height="128">](/screenshots/characterSheet.png)
[<img src="/screenshots/improvisedSpellcasting.png" width="128" height="128">](/screenshots/improvisedSpellcasting.png)

### Supported Sheets
* Mortals (CofD)
* Mages (MtAw 2e)
* Vampires (VtR 2e)
* Changelings (CtL 2e)
* Sleepwalkers
* (Proximi)
* Ephemeral Entities (Goetia, Angel, Ghost, Spirit)

### Current Features

* Sheets for Spells, Active Spells, Attainments, Yantras, Firearms, Melee, Armor, Ammo, Equipment, Merits, Conditions, Tilts, ...
* Tracking of pretty much all the things the original character sheet contains
* Rolling of any combination of Attribute, Skill, and other trait
* Complete dice roller with all rules (e.g. 10-/9-/8-again, rote pools, chance die removing 10-again, and 1-again for rote pools)
* Dice penalties for 0 dots in Abilities
* Intelligent health tracking (kudos to Pecklaaz)
* Reloading system from my TAC System (might be a bit over-the-top but it works), in order to reload you have to have ammo your inventory with the same cartridge as the firearm
* Basic Doors/Impression tracking
* Quick calculation of derived stats
* Weapon rolling
* Mage: (Improvised) spell casting! With almost all features as the one on voidstate.com (except Yantras)!
* Beat tracking system (with history, and reasons, etc.)

### Currently Working On

* Adaptation of the system to Mage the Ascension (Gonna divide it more while I get deeper with the development)


Unfortunately, due to White Wolf's licensing, I'm pretty sure I can't publish a Compendium.
Correct me if I'm wrong.

### Credits

* Icons from https://game-icons.net/ (License: https://creativecommons.org/licenses/by/3.0/)
* Icons from fontawesome (License: https://fontawesome.com/license)
* All other icons/textures are CC0
* Some code snippets were used from the dnd 5e system.
* Thanks to Pecklaaz for some of the GUI work.
